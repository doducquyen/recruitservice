package vn.vnlab.recruit.common;

public class Property {

	public final static String PATH_SYSTEM_PROPERTY = "D:\\Eclipse\\chromedriver.exe";
	public final static String PATH_ORIGIN = "C:/Users/quyendd/workspace/Data/";
	public final static String PATH_DATA_SAMPLE = PATH_ORIGIN + "InfoTemplate.json";
	public final static String PATH_LIST_MEMBER_FILTERED = PATH_ORIGIN + "MemberFiltered.csv";
	public final static String PATH_LINK_PROFILE = PATH_ORIGIN + "LinkProfile.txt";
	public final static String PATH_MESSAGE = PATH_ORIGIN + "Message.txt";
	public final static String PATH_LOG = PATH_ORIGIN + "Log.txt";
	public final static String PATH_GROUP_MEMBER = "members/";

	public final static String HEADER_CSV = "Link Profile,Work,Education";
	public final static String LINK_FB = "http://www.facebook.com";
	public final static String USER = "doducquyen18121995@gmail.com";
	public final static String PASSWORD = "quyen@123";
	public final static String GROUP_NAME = "8e Hedspi k58";
	public final static String MESSAGE = "Xin chào bạn ";

	public final static String ID_USER = "email";
	public final static String ID_PASS = "pass";
	public final static String ID_SUBMIT = "u_0_r";

	public final static String PNREF_WORK = "work";
	public final static String TEXT_WORK = "work";
	public final static String PNREF_EDU = "edu";
	public final static String TEXT_EDU = "edu";
	public final static String TEXT_ABOUT = "About";
	public final static String TEXT_WORK_EDU = "Work and Education";
	public final static String TEXT_GROUP = "Groups";
	public final static String TEXT_MESSAGE = "Message";
	public final static String TEXT_SEE_MORE = "See More";
	public final static String ATTRIBUTE_NAME_PNREF = "data-pnref";
	public final static String CLASS_TEXTFIELD_SEARCH = "_1frb";
	public final static String CLASS_BUTTON_SEARCH = "_51sy";
	public final static String CLASS_WORK_EDU = "_4qm1";
	public final static String ID_USER_NAME = "fb-timeline-cover-name";

	public final static String XPATH_PNREF_WORK = "//div[@data-pnref='work']//ul//li//div//div//div//div//div//div//a";
	public final static String XPATH_PNREF_EDU = "//div[@data-pnref='edu']//ul//li//div//div//div//div//div//div//a";
	public final static String XPATH_GET_LINK_MEMBER = "//div[@class='_6a _5u5j']//div[2]//a";
	public final static String XPATH_JOIN_GROUP = "//div[@class='_52eh _5bcu']//a";

	public final static char DASH = '-';
	public final static char COMMA = ',';
	public final static char SEMICOLON = ';';

}
