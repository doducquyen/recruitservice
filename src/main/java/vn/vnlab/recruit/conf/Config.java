package vn.vnlab.recruit.conf;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import vn.vnlab.recruit.common.Property;

public class Config {

	private static final Config cnf = new Config();
	private List<String> listWork = new ArrayList<String>();
	private List<String> listEdu = new ArrayList<String>();
	
	public List<String> getListWork() {
		return listWork;
	}

	public List<String> getListEdu() {
		return listEdu;
	}

	public static Config getInstance() {
		return cnf;
	}

	private Config() {
		JSONParser parser = new JSONParser();

		try {
			JSONObject obj = (JSONObject) parser.parse(new FileReader(Property.PATH_DATA_SAMPLE));

			JSONArray works = (JSONArray) obj.get(Property.TEXT_WORK);
			@SuppressWarnings("unchecked")
			Iterator<String> iteratorWork = works.iterator();

			while (iteratorWork.hasNext()) {
				listWork.add(iteratorWork.next());
			}

			JSONArray edus = (JSONArray) obj.get(Property.TEXT_EDU);
			@SuppressWarnings("unchecked")
			Iterator<String> iteratorEdu = edus.iterator();

			while (iteratorEdu.hasNext()) {
				listEdu.add(iteratorEdu.next());
			}

		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}

	}
}
