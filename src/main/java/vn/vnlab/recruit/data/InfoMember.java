package vn.vnlab.recruit.data;

import org.apache.commons.lang3.StringUtils;

import vn.vnlab.recruit.conf.Config;

public class InfoMember {
	public String link;
	public String work;
	public String edu;

	public InfoMember() {
		this.link = "";
		this.work = "";
		this.edu = "";
	}

	public InfoMember(String link, String work, String edu) {
		this.link = link;
		this.work = work;
		this.edu = edu;
	}

	public boolean isValid() {
		if (!StringUtils.isEmpty(work) || !StringUtils.isEmpty(edu)) {
			if (filterInfo())
				return true;
		}

		return false;
	}

	public String toCSV() {

		return link + "," + work + "," + edu;
	}
	
	public boolean filterInfo() {

		boolean checkPerson = false;

		for (int i = 0; i < Config.getInstance().getListWork().size(); i++) {
			String workSampleLower = Config.getInstance().getListWork().get(i);
			checkPerson = (checkPerson == false) ? work.toLowerCase().contains(workSampleLower) : checkPerson;
		}

		if (checkPerson == false)
			for (int i = 0; i < Config.getInstance().getListEdu().size(); i++) {
				String eduSampleLower = Config.getInstance().getListEdu().get(i);
				checkPerson = (checkPerson == false) ? edu.toLowerCase().contains(eduSampleLower) : checkPerson;
			}
		return checkPerson;
	}
}
