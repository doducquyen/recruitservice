package vn.vnlab.recruit.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;

import vn.vnlab.recruit.common.Property;
import vn.vnlab.recruit.main.BrowseWeb;
import vn.vnlab.recruit.utils.FileInfoTemplate;
import vn.vnlab.recruit.utils.FileLink;

public class InfoTemplate {

	public void update() throws InterruptedException, FileNotFoundException, IOException, ParseException {
		WebDriver driver = BrowseWeb.loginFacebook();
		HashMap<Integer, String> hmapLinks = FileLink.get();

		JSONArray jArrWork = new JSONArray();
		JSONArray jArrEdu = new JSONArray();
		JSONParser parser = new JSONParser();
		boolean isExistFile = new File(Property.PATH_DATA_SAMPLE).exists();

		if (isExistFile) {
			try {
				Object obj = parser.parse(new FileReader(Property.PATH_DATA_SAMPLE));
				JSONObject jsonObject = (JSONObject) obj;
				jArrWork = (JSONArray) jsonObject.get(Property.TEXT_WORK);
				jArrEdu = (JSONArray) jsonObject.get(Property.TEXT_EDU);
			} catch (Exception e) {

			}
		}
		
		InfoMember info = new InfoMember();
		
		for (int i = 0; i < hmapLinks.size(); i++) {
			driver.navigate().to(hmapLinks.get(i));
			Thread.sleep(2000);
			info = BrowseWeb.getInfoFromGroup(driver, info);
			
			try {
				FileInfoTemplate.save(info, jArrWork, jArrEdu);
			} catch (IOException | ParseException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		InfoTemplate infoTemplate = new InfoTemplate();
		try {
			infoTemplate.update();
			System.out.println("Get information template done");
		} catch (InterruptedException | IOException | ParseException e) {
			e.printStackTrace();
		}
	}
}
