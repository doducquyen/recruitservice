package vn.vnlab.recruit.data;

import org.openqa.selenium.WebDriver;

import vn.vnlab.recruit.common.Property;
import vn.vnlab.recruit.main.BrowseWeb;
import vn.vnlab.recruit.utils.DataObject;

public class LinkProfile {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver = BrowseWeb.loginFacebook();
		BrowseWeb.getLinkFromGroup(Property.GROUP_NAME, driver);
		
		DataObject.writeLog("{LinkProfile.java} Saved links from group " + Property.GROUP_NAME);
		System.out.println("Get links done");
	}
}
