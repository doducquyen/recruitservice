package vn.vnlab.recruit.main;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import vn.vnlab.recruit.common.Property;
import vn.vnlab.recruit.data.InfoMember;
import vn.vnlab.recruit.utils.FileLink;

public class BrowseWeb {

	public static WebDriver loginFacebook() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", Property.PATH_SYSTEM_PROPERTY);
		Map<String, Object> prefs = new HashMap<String, Object>();

		// Put this into prefs map to switch off browser notification
		prefs.put("profile.default_content_setting_values.notifications", 2);
		prefs.put("credentials_enable_service", Boolean.valueOf(false));
		prefs.put("profile.password_manager_enabled", Boolean.valueOf(false));

		// Create chrome options to set this prefs
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);

		// Now initialize chrome driver with chrome options which will switch
		// off this browser notification on the chrome browser
		WebDriver driver = new ChromeDriver(options);

		driver.get(Property.LINK_FB);
		Thread.sleep(3000);

		WebElement element1 = driver.findElement(By.id(Property.ID_USER));
		element1.sendKeys(Property.USER);
		Thread.sleep(3000);
		
		WebElement element2 = driver.findElement(By.id(Property.ID_PASS));
		element2.sendKeys(Property.PASSWORD);
		Thread.sleep(3000);

		WebElement element3 = driver.findElement(By.id(Property.ID_SUBMIT));
		element3.click();
		Thread.sleep(3000);
		
		return driver;
	}

	public static void getLinkFromGroup(String groupName, WebDriver driver) throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.className(Property.CLASS_TEXTFIELD_SEARCH)).sendKeys(groupName);
		Thread.sleep(3000);
		driver.findElement(By.className(Property.CLASS_BUTTON_SEARCH)).submit();
		driver.findElement(By.linkText(Property.TEXT_GROUP)).click();
		driver.findElement(By.xpath(Property.XPATH_JOIN_GROUP)).click();

		String url_member = driver.getCurrentUrl().concat(Property.PATH_GROUP_MEMBER);
		driver.navigate().to(url_member);

		// scroll and click see more member
		while (driver.findElements(By.linkText(Property.TEXT_SEE_MORE)).size() != 0) {
			driver.findElement(By.linkText(Property.TEXT_SEE_MORE)).click();
			Thread.sleep(2000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,250)", "");
			Thread.sleep(2000);
		}

		// get link fb of member and write to file
		List<WebElement> allElements = driver.findElements(By.xpath(Property.XPATH_GET_LINK_MEMBER));
		FileLink.save(allElements);
	}
	
	public static InfoMember getInfoFromGroup(WebDriver driver, InfoMember info) throws InterruptedException {
				
		driver.findElement(By.linkText(Property.TEXT_ABOUT)).click();
		Thread.sleep(2000);
		driver.findElement(By.linkText(Property.TEXT_WORK_EDU)).click();
		Thread.sleep(2000);

		List<WebElement> fields = driver.findElements(By.className(Property.CLASS_WORK_EDU));
		
		for (WebElement field : fields) {
			String pnref = field.getAttribute(Property.ATTRIBUTE_NAME_PNREF);
			if (pnref != null) {
				if (pnref.compareTo(Property.PNREF_WORK) == 0) {
					List<WebElement> elements = field.findElements(By.xpath(Property.XPATH_PNREF_WORK));
					for (WebElement e : elements) {
						info.work = info.work.concat(e.getText().replace(Property.COMMA, Property.DASH));
						info.work = info.work.concat(String.valueOf(Property.SEMICOLON));
					}
				} else if (pnref.compareTo(Property.PNREF_EDU) == 0) {
					List<WebElement> elements = field.findElements(By.xpath(Property.XPATH_PNREF_EDU));
					for (WebElement e : elements) {
						info.edu = info.edu.concat(e.getText().replace(Property.COMMA, Property.DASH));
						info.edu = info.edu.concat(String.valueOf(Property.SEMICOLON));
					}
				}
			}
		}
		return info;
	}
}
