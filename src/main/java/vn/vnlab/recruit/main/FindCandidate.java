package vn.vnlab.recruit.main;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;

import vn.vnlab.recruit.data.InfoMember;
import vn.vnlab.recruit.main.BrowseWeb;
import vn.vnlab.recruit.utils.DataObject;
import vn.vnlab.recruit.utils.FileLink;
import vn.vnlab.recruit.utils.FileMemberFiltered;

public class FindCandidate {

	final static Integer MAX_LINE_READ = 20;

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver = BrowseWeb.loginFacebook();
		HashMap<Integer, String> hmapLinks = FileLink.get();
		List<InfoMember> infos = new ArrayList<InfoMember>();
		int countPer = 0;

		for (int i = 0; i < hmapLinks.size(); i++) {
			try {
				InfoMember info = new InfoMember();
				info.link = hmapLinks.get(i);
				driver.navigate().to(hmapLinks.get(i));
				Thread.sleep(2000);
				info = BrowseWeb.getInfoFromGroup(driver, info);

				if (info.isValid()) {
					infos.add(info);
				}
				countPer = i + 1;

				if (countPer % MAX_LINE_READ == 0 || countPer == hmapLinks.size()) {
					FileMemberFiltered.save((ArrayList<InfoMember>) infos);
					infos = new ArrayList<InfoMember>();
				}
			} catch (Exception e) {
				if (countPer % MAX_LINE_READ != 0) {
					countPer = countPer - countPer % MAX_LINE_READ;
				}

				DataObject.writeLog("{FindCandidate.java} Find candidate interrupted in line " + countPer
						+ " of file LinkProfile.txt");
				System.out.println("Find candidate interrupted");
			}
		}
		DataObject.writeLog("{FindCandidate.java} Scanned " + countPer + " persons");
		System.out.println("Find candidate done");
	}
}