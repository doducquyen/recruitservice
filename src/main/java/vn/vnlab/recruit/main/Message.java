package vn.vnlab.recruit.main;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import vn.vnlab.recruit.common.Property;
import vn.vnlab.recruit.utils.DataObject;
import vn.vnlab.recruit.utils.FileMemberFiltered;

public class Message {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = BrowseWeb.loginFacebook();
		List<String> listLink = FileMemberFiltered.getLink();
		int countPer = 0;
		for (int i = 0; i < listLink.size(); i++) {
			driver.navigate().to(listLink.get(i));
			Thread.sleep(2000);
			try {
				String userName = driver.findElement(By.id(Property.ID_USER_NAME)).getText();
				String msg = Property.MESSAGE + userName + DataObject.getMessage();

				driver.findElement(By.linkText(Property.TEXT_MESSAGE)).click();
				Thread.sleep(2000);
				WebElement msgBox = driver.switchTo().activeElement();
				msgBox.sendKeys(msg);
				msgBox.sendKeys(Keys.ENTER);
				Thread.sleep(2000);
				countPer = i + 1;

			} catch (Exception e) {
				DataObject.writeLog("{Message.java} Send message interrupted when try to send to link " + countPer
						+ " of file MemberFiltered.csv.");
				System.out.println("Sent message interrupt.");
			}
		}
		DataObject.writeLog("{Message.java} Send message done.");
		System.out.println("Send message done.");
	}
}
