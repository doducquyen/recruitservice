package vn.vnlab.recruit.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import vn.vnlab.recruit.common.Property;

public class DataObject {

	public static String getMessage() {
		try {

			FileReader fr = new FileReader(new File(Property.PATH_MESSAGE));
			BufferedReader br = new BufferedReader(fr);
			StringBuilder sb = new StringBuilder();
			String line;

			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}

			fr.close();
			return sb.toString();

		} catch (Exception ex) {
			return null;
		}
	}

	public static void writeLog(String log) {

		try (FileWriter fw = new FileWriter(Property.PATH_LOG, true);
				BufferedWriter writer = new BufferedWriter(fw);
				PrintWriter pw = new PrintWriter(writer)) {
			
			Date curDate = new Date(); 
			writer.append(curDate + ":: " + log + "\n");
			pw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	

	
}
