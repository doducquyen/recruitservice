package vn.vnlab.recruit.utils;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import vn.vnlab.recruit.common.Property;
import vn.vnlab.recruit.data.InfoMember;

public class FileInfoTemplate {

	@SuppressWarnings("unchecked")
	public static void save(InfoMember info, JSONArray jArrWork, JSONArray jArrEdu)
			throws FileNotFoundException, IOException, ParseException {
		
		JSONObject objInfo = new JSONObject();
		String[] listWork = info.work.split(String.valueOf(Property.SEMICOLON));
		for (int i = 0; i < listWork.length; i++) {
			if (!jArrWork.contains(listWork[i].toLowerCase()))
				jArrWork.add(listWork[i].toLowerCase());
		}
		objInfo.put(Property.TEXT_WORK, jArrWork);

		String[] listEdu = info.edu.split(String.valueOf(Property.SEMICOLON));
		for (int i = 0; i < listEdu.length; i++) {
			if (!jArrEdu.contains(listEdu[i].toLowerCase()))
				jArrEdu.add(listEdu[i].toLowerCase());
		}
		objInfo.put(Property.TEXT_EDU, jArrEdu);

		try (FileWriter fw = new FileWriter(Property.PATH_DATA_SAMPLE);
				BufferedWriter writer = new BufferedWriter(fw);
				PrintWriter pw = new PrintWriter(writer)) {

			writer.write(objInfo.toJSONString());
			pw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
