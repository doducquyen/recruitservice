package vn.vnlab.recruit.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebElement;

import vn.vnlab.recruit.common.Property;

public class FileLink {
	
	public static HashMap<Integer, String> get() {
		try {

			File f = new File(Property.PATH_LINK_PROFILE);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			HashMap<Integer, String> hmapLinks = new HashMap<Integer, String>();
			String line;
			int i = 0;

			while ((line = br.readLine()) != null) {
				hmapLinks.put(i, line);
				i++;
			}
			fr.close();

			return hmapLinks;
		} catch (Exception ex) {

			return null;
		}
	}

	public static void save(List<WebElement> allElements) {
		try {

			FileWriter fw = new FileWriter(new File(Property.PATH_LINK_PROFILE), true);
			HashMap<Integer, String> hmapLinks = get();

			for (WebElement element : allElements) {
				String link = element.getAttribute("href");
				// get link origin
				if (link.contains("&"))
					link = link.split("&")[0];
				else if (link.contains("?"))
					link = link.split("\\?")[0];

				if (!hmapLinks.containsValue(link)) {
					fw.write(link + "\n");
				}
			}

			fw.close();
		} catch (IOException ex) {
		}
	}	
}
