package vn.vnlab.recruit.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import vn.vnlab.recruit.common.Property;
import vn.vnlab.recruit.data.InfoMember;

public class FileMemberFiltered {

	public static List<String> getLink() {
		try {

			FileReader fr = new FileReader(new File(Property.PATH_LIST_MEMBER_FILTERED));
			BufferedReader br = new BufferedReader(fr);
			List<String> listLink = new ArrayList<String>();
			String line = br.readLine();

			while ((line = br.readLine()) != null) {
				String[] link = line.split(",");
				listLink.add(link[0]);
			}

			fr.close();
			return listLink;

		} catch (Exception ex) {
			return null;
		}
	}

	public static void save(ArrayList<InfoMember> infos) {

		boolean isExistFile = new File(Property.PATH_LIST_MEMBER_FILTERED).exists();

		try (FileWriter fw = new FileWriter(Property.PATH_LIST_MEMBER_FILTERED, true);
				BufferedWriter writer = new BufferedWriter(fw);
				PrintWriter pw = new PrintWriter(writer)) {
			if (!isExistFile) {
				writer.append(Property.HEADER_CSV + "\n");
			}

			// write out records
			for (InfoMember info : infos) {
				if (!getLink().contains(info.link))
					writer.append(info.toCSV() + "\n");
			}
			pw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
